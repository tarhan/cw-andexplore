package com.commonsware.todo.ui.roster

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.commonsware.todo.repo.FilterMode
import com.commonsware.todo.repo.ToDoModel
import com.commonsware.todo.repo.ToDoRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RosterViewState(
  val items: List<ToDoModel> = listOf(),
  val filterMode: FilterMode = FilterMode.ALL
)

class RosterMotor(private val repo: ToDoRepository): ViewModel() {
  private val _states = MediatorLiveData<RosterViewState>()
  val states: LiveData<RosterViewState> = _states
  private var lastSource: LiveData<List<ToDoModel>>? = null

  init {
    load(FilterMode.ALL)
  }

  fun load(filterMode: FilterMode) {
    lastSource?.let { _states.removeSource(it) }

    val items = repo.items(filterMode)

    _states.addSource(items) { models ->
      _states.value = RosterViewState(models, filterMode)
    }

    lastSource = items
  }

  fun save(model: ToDoModel) {
    viewModelScope.launch(Dispatchers.Main) {
      repo.save(model)
    }
  }
}